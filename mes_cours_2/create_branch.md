# Concepte de branche
Si on travail sur un projet de plusieurs personnes, on ne peut pas se permettre de modifier directement les fichiers du projet principal qu'on appelle master. Ainsi pour eviter cela on créer des branches qui n'est rien d'autre qu'une copie d'un des fichiers du master qu'on pourra modifier et demander la validation des modifications apporter.
Pour créer sa blanche:
<ul>
<li>On ouvre son fichier, on clique sur master en bas à droite sur webstorm</li>
On selectionne new branche qu'on va donner un nom.
</ul>
La branche est créer on peut apporter des modifications sur le fichier ensuite on commite et push.

Une fois finit on demande l'autorisation  de merge c'est à dire d'assemblé son travail avec le master.Pour se faire
<ul>
<li>On va sur git à la page principale du projet, on clique sur merge request</li>
<li>On assigne le chef de projet</li>
<li>Puis on fait submit request</li>
</ul>
Et le chef de projet s'en charge ppur valider la modification en cliquant sur merge.
Une fois le fichier mergé on retourne webstorm on intégre le fichier en repassant sur master puis checkout et updead.