# Boostrap et modification

Pour modifier un fichier dans Bootstrap il existe deux manières:

<ol>
<li>Faire des thèmes : cette méthode demande des connaissance poussées en CSS, qui est un travail plutot réservé aux intégrateurs.</li>
<li>Modifier à partir d'une feuille de style:<br> On crée une feuille de style css où on fait ses modifications. Puis on fait le lien entre le fichier html et le fichier css en indiquant le bon chemin pour que ces modifications soient prises en compte.</li>
</ol>

## Organiser CSS
Dans un projet on peut avoir plusieurs pages à réliser et beaucoups de modificatios à apporter dans ses pages. C'est pourquoi on peut organiser sa feuille de style principale en plusieurs feuilles.<br>
On créer d'abord un autre fichier css où on met les modifications qu'on veut et faire le lien entre ce nouveau fichier et le fichier principal en mettant dans le css principal <br>
@import url('nom du nouveau fichier css). ET le lien est créer les modifications faites dans ce nouveaux fichiers seront pris en compte dans la page html. 
